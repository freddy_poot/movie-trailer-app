# iOS Movie Trailer App

Su objetivo es el desarrollo de una aplicación de trailer de peliculas compatible con iPhone y iPad, utilizando la api gratuita de peliculas [TMDb]

# Requisitos

- La aplicación debe estar construida con Swift.
- El entregable debe contener pasos completos para configurar un entorno para ejecutar la aplicación. Suponiendo que el evaluador no es desarrollador.

# Historias

#### Autenticación
 - El usuario debe autenticarse con correo electronico y contraseña. 
 - El usuario debe poder cerrar sesión.
 

*Puede usar datos falsos y un endpoint falso para validar lo que escribe el usuario en el formulario, el diseño de la interfaz de esta historia queda a su criterio, la sesión del usuario autenticado debe ser almacenada localmente*

#### Dashboard (Vista principal)
 - El usuario debe poder ver un tablero con la lista de peliculas próximas por estrenar (Mostrar solo las primeras 5), las mas populares y mejor calificadas. El tablero se verá así.
 
![Dashboard](assets/dashboard.png)

*La lista de peliculas debe almacenarse localmente (no es necesario almacenar todas las peliculas, solo algunos de cada lista.).*

#### Detalle de la pelicula
 - El usuario debe poder ver el detalle de la pelicula. La pantalla del detalle de la pelicula se verá asi.
 
![Dashboard](assets/movie_detail.png)

#### Trailer de la pelicula
 - El usuario debe poder visualizar el trailer de la pelicula. 

*Para esta pantalla debe integrar SFSafariViewController para acceder a la url del video*
 
#### Puntos extra
 - Utilice una arquitectura para dividir y encapsular cada capa de la aplicación, por ejemplo MVC, MVP, MVVM.
 - Utilice los principios solid durante el desarrollo del proyecto.
 - Crear un conjunto de pruebas unitarias para el proyecto.
 - Crear un conjunto de pruebas de UI para el proyecto.
 
[TMDb]: <https://developers.themoviedb.org/3/getting-started/introduction>
